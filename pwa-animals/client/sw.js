const cacheName = "hello-pwa";
self.importScripts("./js/fetchGQL.js");
self.importScripts("./js/idb.js");
const filesToCache = [
  "/",
  "./index.html",
  "./css/style.css",
  "./js/main.js",
  "./images/bg.png",
  "./fa/css/all.css",
];

/* Start the service worker and cache all of the app's content */
self.addEventListener("install", (e) => {
  try {
    e.waitUntil(async () => {
      const cache = await caches.open(cacheName);
      return cache.addAll(filesToCache);
    });
  } catch (error) {
    console.log("error", error);
  }
});

/* Serve cached content when offline */
self.addEventListener("fetch", (e) => {
  try {
    e.respondWith(async () => {
      const c = await caches.match(e.request);
      return c || fetch(e.request);
    });
  } catch (error) {
    console.log("error", error);
  }
});

self.onsync = (event) => {
  if (event.tag == "animal-sync") {
    event.waitUntil(
      (async () => {
        const local = await getLocalAnimals();
        local.forEach(async (it) => {
          const res = await saveAnimal(it);
          console.log("Sync res", res);
          if (res !== false) {
            deleteLocalAnimal(it.id);
          }
        });
      })()
    );
  }
};
