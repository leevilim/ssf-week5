const main = async () => {
  const elementAnimalList = document.querySelector("#list");
  const elementSpeciesSelect = document.querySelector("#select");

  const initList = async () => {
    elementAnimalList.replaceChildren();
    try {
      const species = await getSpecies();
      updateLocalSpecies(species);
      await updateAnimalCacheList(await getAnimals());
      const animals = await getAnimalCacheList();

      species.forEach((species) => {
        const option = document.createElement("option");
        option.text = species.speciesName;
        option.value = species.id;

        elementSpeciesSelect.appendChild(option);
      });

      animals.forEach((it) => {
        const li = document.createElement("li");
        li.innerHTML = `<h3>${it.animalName}</h3> <p>${
          it.species?.speciesName || ""
        }</p> <p>${it.species?.category?.categoryName || ""}</p>`;
        elementAnimalList.appendChild(li);
      });
    } catch (error) {
      console.log("Error", error);
    }
  };

  initList();

  if (navigator.serviceWorker) {
    navigator.serviceWorker
      .register("./sw.js")
      .then(() => {
        return navigator.serviceWorker.ready;
      })
      .then((registration) => {
        document.getElementById("btn").addEventListener("click", async (e) => {
          e.preventDefault();
          const animalName = document.querySelector("#animalNameInput").value;
          const speciesId = elementSpeciesSelect.value;

          const animal = {
            animalName,
            speciesId,
          };

          await saveAnimalLocal(animal);
          await updateAnimalCacheList([
            ...(await getAnimalCacheList()),
            animal,
          ]);
          document.querySelector("#animalNameInput").value = "";
          elementSpeciesSelect.value = "";
          initList();

          if (registration.sync) {
            registration.sync.register("animal-sync").catch((err) => {
              console.log("failed to reg animal-sync", err);
            });
          } else {
            if (navigator.onLine) {
              saveAnimal(msg);
            } else {
              alert(
                "You are offline! When your internet returns, we'll finish up your request."
              );
            }
          }
        });
      });
  }
};

main();
