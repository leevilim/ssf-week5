const fetchGraphql = async (query) => {
  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify(query),
  };
  try {
    const response = await fetch("http://localhost:3000/graphql", options);
    const json = await response.json();
    console.log(json.data);
    return json.data;
  } catch (e) {
    console.log(e);
    return false;
  }
};

const saveAnimal = async (animal) => {
  const query = {
    query: `
            mutation VariableTest($animalName: String!, $speciesId: String!) {
              addAnimal(animalName: $animalName, species: $speciesId) {
                id
              }
            }`,
    variables: animal,
  };
  const data = await fetchGraphql(query);
  return data.addAnimal;
};

const getSpecies = async () => {
  const otherQuery = {
    query: `
            {
              species {
                id
                speciesName
              }
            }`,
  };
  const data = await fetchGraphql(otherQuery);
  return data.species;
};

const getAnimals = async () => {
  const otherQuery = {
    query: `
            {
              animals {
                id
                animalName
                species {
                    id
                    speciesName
                    category {
                        categoryName
                    }
                }
              }
            }`,
  };
  const data = await fetchGraphql(otherQuery);
  return data.animals;
};
