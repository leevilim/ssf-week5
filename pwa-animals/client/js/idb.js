"use strict";
let db;

const setup = () => {
  const request = indexedDB.open("animal_db");

  request.onsuccess = (event) => {
    db = event.target.result;
    console.log("Db ok");
  };

  request.onerror = (event) => {
    console.error("Db error", event);
  };

  request.onupgradeneeded = (event) => {
    const database = event.target.result;
    console.log("onupgradeneeded");

    const animalTempObjStore = database.createObjectStore("animal", {
      keyPath: "id",
      autoIncrement: true,
    });
    animalTempObjStore.createIndex("animalName", "animalName", {
      unique: false,
    });
    animalTempObjStore.createIndex("speciesId", "speciesId", { unique: false });

    const animalCacheListStore = database.createObjectStore("animalCache", {
      keyPath: "id",
      autoIncrement: true,
    });
    animalCacheListStore.createIndex("animalName", "animalName", {
      unique: false,
    });
    animalCacheListStore.createIndex("speciesId", "speciesId", {
      unique: false,
    });

    const speciesObjStore = database.createObjectStore("species", {
      keyPath: "id",
    });
    speciesObjStore.createIndex("speciesName", "speciesName", {
      unique: false,
    });
    speciesObjStore.createIndex("id", "id", { unique: true });
  };
};
setup();

const saveAnimalLocal = (animal) => {
  return new Promise((resolve, reject) => {
    const transaction = db.transaction(["animal"], "readwrite");

    transaction.oncomplete = (event) => {
      console.log("All transaction events done!");
      resolve(event);
    };

    transaction.onerror = (event) => {
      console.log("Transaction error", event);
      reject(event);
    };

    const objectStore = transaction.objectStore("animal");
    const request = objectStore.add({ ...animal, synced: false });
    request.onsuccess = (event) => {
      event.data === animal;
    };
  });
};

const getLocalAnimals = async () => {
  return new Promise((resolve, reject) => {
    const db = indexedDB.open("animal_db");
    db.onsuccess = function (event) {
      this.result
        .transaction("animal", "readonly")
        .objectStore("animal")
        .getAll().onsuccess = (event) => {
        resolve(event.target.result);
      };
    };
    db.onerror = (err) => {
      reject(err);
    };
  });
};

const deleteLocalAnimal = async (id) => {
  return new Promise((resolve, reject) => {
    const db = indexedDB.open("animal_db");
    db.onsuccess = function (event) {
      this.result
        .transaction("animal", "readwrite")
        .objectStore("animal")
        .delete(id).onsuccess = (event) => {
        resolve(event.target.result);
      };
    };
    db.onerror = (err) => {
      reject(err);
    };
  });
};

const updateLocalSpecies = async (speciesList) => {
  if (!speciesList) {
    return;
  }
  return new Promise((resolve, reject) => {
    const transaction = db.transaction(["species"], "readwrite");

    transaction.oncomplete = (event) => {
      console.log("All transaction events done!");
      resolve(event);
    };

    transaction.onerror = (event) => {
      console.log("Transaction error", event);
      reject(event);
    };

    const objectStore = transaction.objectStore("species");
    objectStore.clear();
    speciesList.forEach((it) => {
      const request = objectStore.add(it);
      request.onsuccess = (event) => {
        console.log("Saved species");
      };
    });
  });
};

const getLocalSpecies = () => {
  return new Promise((resolve, reject) => {
    const db = indexedDB.open("animal_db");
    db.onsuccess = function (event) {
      this.result
        .transaction("species", "readonly")
        .objectStore("species")
        .getAll().onsuccess = (event) => {
        resolve(event.target.result);
      };
    };
    db.onerror = (err) => {
      reject(err);
    };
  });
};

const updateAnimalCacheList = async (list) => {
  if (!list) {
    return;
  }
  return new Promise((resolve, reject) => {
    const transaction = db.transaction(["animalCache"], "readwrite");

    transaction.oncomplete = (event) => {
      console.log("All transaction events done!");
      resolve(event);
    };

    transaction.onerror = (event) => {
      console.log("Transaction error", event);
      reject(event);
    };

    const objectStore = transaction.objectStore("animalCache");
    objectStore.clear();
    list.forEach((it) => {
      const request = objectStore.add(it);
      request.onsuccess = (event) => {};
    });
  });
};

const getAnimalCacheList = async () => {
  return new Promise((resolve, reject) => {
    const db = indexedDB.open("animal_db");
    db.onsuccess = function (event) {
      this.result
        .transaction("animalCache", "readonly")
        .objectStore("animalCache")
        .getAll().onsuccess = (event) => {
        resolve(event.target.result);
      };
    };
    db.onerror = (err) => {
      reject(err);
    };
  });
};
