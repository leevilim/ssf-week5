import Species from "../models/speciesModel.js";
import Category from "../models/categoryModel.js";

export default {
  Query: {
    species: async (parent, args) => {
      return await Species.find({});
    },
  },

  Species: {
    category: async (parent, _) => {
      const id = parent.category;
      return await Category.findById(id).exec();
    },
  },

  Mutation: {
    addSpecies: (_, args) => {
      const species = new Species(args);
      return species.save();
    },
  },
};
