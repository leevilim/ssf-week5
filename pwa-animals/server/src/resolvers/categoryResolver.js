import Category from "../models/categoryModel.js";

export default {
  Mutation: {
    addCategory: (_, args) => {
      const newCategory = new Category(args);
      return newCategory.save();
    },
  },
};
