import { gql } from "apollo-server-express";
import Species from "./speciesSchema.js";

export default gql`
  extend type Query {
    animals: [Animal]
  }

  type Animal {
    id: ID
    animalName: String
    species: Species
  }

  extend type Mutation {
    addAnimal(animalName: String, species: String): Animal
    modifyAnimal(id: String, animalName: String, species: String): Animal
  }
`;
