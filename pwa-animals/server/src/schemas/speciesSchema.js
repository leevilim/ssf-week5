import { gql } from "apollo-server-express";

export default gql`
  extend type Query {
    species: [Species]
  }

  type Species {
    id: ID
    speciesName: String
    category: Category
  }

  extend type Mutation {
    addSpecies(speciesName: String, category: String): Species
  }
`;
