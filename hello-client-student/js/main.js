"use strict";

window.onload = async () => {
  "use strict";
};

(async () => {
  const ul = document.querySelector("ul");
  const rfrsh = document.querySelector("#refresh");
  const form = document.querySelector("form");
  const username = "asg334";
  const greeting = form.elements.greeting;

  const init = async () => {
    const data = [];
    try {
      const greetings = await getGreetingsByUser(username);
      for (const message of greetings) {
        data.push(message);
      }
      console.log("Greetings returned", greetings);
    } catch (e) {
      console.log(e.message);
    }

    ul.innerHTML = "";
    data.forEach((item) => {
      ul.innerHTML += `<ul>${item.username}: ${item.greeting}</ul>`;
    });
  };

  init();

  if (navigator.serviceWorker) {
    navigator.serviceWorker
      .register("./sw.js")
      .then(() => {
        return navigator.serviceWorker.ready;
      })
      .then((registration) => {
        document.getElementById("send").addEventListener("click", async (e) => {
          e.preventDefault();
          const msg = {
            username,
            greeting: greeting.value,
          };

          await saveMsgLocally(msg);

          if (registration.sync) {
            registration.sync.register("msg-sync").catch((err) => {
              console.log("failed to reg msg-sync", err);
            });
          } else {
            if (navigator.onLine) {
              saveGreeting(msg);
            } else {
              alert(
                "You are offline! When your internet returns, we'll finish up your request."
              );
            }
          }
        });
      });
  }

  rfrsh.addEventListener("click", init);
})();
