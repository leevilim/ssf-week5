"use strict";
let db;

const setup = () => {
  const request = indexedDB.open("db_msg");

  request.onsuccess = (event) => {
    db = event.target.result;
    console.log("Db ok");
  };

  request.onerror = (event) => {
    console.error("Db error", event);
  };

  request.onupgradeneeded = (event) => {
    const database = event.target.result;
    console.log("onupgradeneeded");

    const msgObjectStore = database.createObjectStore("msg", {
      keyPath: "id",
      autoIncrement: true,
    });

    msgObjectStore.createIndex("username", "username", { unique: false });
    msgObjectStore.createIndex("greeting", "greeting", { unique: false });

    // Use transaction oncomplete to make sure the objectStore creation is
    // finished before adding data into it.
    msgObjectStore.transaction.oncomplete = (event) => {
      console.log("init transaction done");
    };
  };
};
setup();

const saveMsgLocally = (msg) => {
  return new Promise((resolve, reject) => {
    const transaction = db.transaction(["msg"], "readwrite");

    transaction.oncomplete = (event) => {
      console.log("All transaction events done!");
      resolve(event);
    };

    transaction.onerror = (event) => {
      console.log("Transaction error", event);
      reject(event);
    };

    const objectStore = transaction.objectStore("msg");
    const request = objectStore.add(msg);
    request.onsuccess = (event) => {
      event.data === msg;
      console.log("Saved msg");
    };
  });
};

const getLocalMsgs = async () => {
  return new Promise((resolve, reject) => {
    const db = indexedDB.open("db_msg");
    db.onsuccess = function (event) {
      this.result
        .transaction("msg", "readonly")
        .objectStore("msg")
        .getAll().onsuccess = (event) => {
        resolve(event.target.result);
      };
    };
    db.onerror = (err) => {
      reject(err);
    };
  });
};

const deleteLocalMsg = async (id) => {
  return new Promise((resolve, reject) => {
    const db = indexedDB.open("db_msg");
    db.onsuccess = function (event) {
      this.result
        .transaction("msg", "readwrite")
        .objectStore("msg")
        .delete(id).onsuccess = (event) => {
        resolve(event.target.result);
      };
    };
    db.onerror = (err) => {
      reject(err);
    };
  });
};
